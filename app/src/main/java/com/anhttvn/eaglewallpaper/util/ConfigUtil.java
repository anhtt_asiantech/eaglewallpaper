package com.anhttvn.eaglewallpaper.util;

public class ConfigUtil {
    public static final String FOLDER_DOWNLOAD = "EAGLE_WALLPAPER";
    public static final String FOLDER_ASSETS = "wallpaper";
    /**
     * api
     */
    public static String KEY_NOTIFICATION = "Notification";

    public static String KEY_WALLPAPER = "Wallpaper";
    public static String INFORMATION ="Information";

//  Config Database

    public static  final String DATABASE = "Eaglewallpaper.db";
    public static  final int VERSION = 3;
}
