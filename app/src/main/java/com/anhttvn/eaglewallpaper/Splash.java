package com.anhttvn.eaglewallpaper;

import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Typeface;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import com.anhttvn.eaglewallpaper.databinding.ActivitySplashBinding;
import com.anhttvn.eaglewallpaper.util.ConfigUtil;
import com.anhttvn.eaglewallpaper.util.Gdpr;
import com.anhttvn.wallpaperlib.database.ConfigData;
import com.anhttvn.wallpaperlib.ui.HomeActivity;
import com.anhttvn.wallpaperlib.util.BaseActivity;
import com.anhttvn.wallpaperlib.util.Config;
import com.anhttvn.wallpaperlib.util.Connectivity;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.InstallStateUpdatedListener;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.InstallStatus;
import com.google.android.play.core.install.model.UpdateAvailability;

public class Splash extends BaseActivity {
    private ActivitySplashBinding splashBinding;
    private static final int TIMEOUT_MILLIS = 2000;
    private static final int MY_REQUEST_CODE = 500;

    private AppUpdateManager appUpdateManager;
    private Handler handler;
    private Runnable runnable;
    @Override
    public void init() {
        appUpdateManager = AppUpdateManagerFactory.create(this);
        Config.FOLDER_DOWNLOAD = ConfigUtil.FOLDER_DOWNLOAD;
        Config.KEY_NOTIFICATION = ConfigUtil.KEY_NOTIFICATION;
        Config.KEY_WALLPAPER = ConfigUtil.KEY_WALLPAPER;
        Config.INFORMATION = ConfigUtil.INFORMATION;
        ConfigData.DATABASE = ConfigUtil.DATABASE;
        ConfigData.VERSION = ConfigUtil.VERSION;

        Config.APP_NAME = this.getString(R.string.app_name);
        Config.FOLDER_ASSETS = ConfigUtil.FOLDER_ASSETS;
        Config.BANNER_WALLPAPER = R.drawable.wallpaper;

        // Banner
        Config.BANNER_ID_ADS_APPLOVIN = this.getString(R.string.banner);
        // Interstitial
        Config.FULL_ID_ADS_APPLOVIN = this.getString(R.string.interstitial);
        // Native
        Config.NATIVE_ID = this.getString(R.string.nativeId);
        // Mrc
        Config.MREC_ID = this.getString(R.string.mrecId);

        Gdpr gdpr = new Gdpr(this);
        gdpr.setGdpr();

        // set font
        Typeface font = Typeface.createFromAsset(this.getAssets(), "font/OngDo.ttf");
        splashBinding.next.setTypeface(font);

        if (Connectivity.isConnected(this)) {
            checkForUpdate();
        } else {
            goHome();
        }

        // handle screen activity home
        runnable = () -> {
            Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        };

        splashBinding.next.setOnClickListener(v -> {
            if (handler != null && runnable != null) {
                handler.removeCallbacks(runnable);
            }

            Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        });


    }

    @Override
    public View contentView() {
        splashBinding = ActivitySplashBinding.inflate(getLayoutInflater());
        return splashBinding.getRoot();
    }


    private void checkForUpdate() {


// Returns an intent object that you use to check for an update.
        Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();

// Checks that the platform will allow the specified type of update.
        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                    // This example applies an immediate update. To apply a flexible update
                    // instead, pass in AppUpdateType.FLEXIBLE
                    && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)) {
                // Request the update.

                try {
                    appUpdateManager.startUpdateFlowForResult(
                            // Pass the intent that is returned by 'getAppUpdateInfo()'.
                            appUpdateInfo,
                            // Or 'AppUpdateType.FLEXIBLE' for flexible updates.
                            AppUpdateType.IMMEDIATE,
                            // The current activity making the update request.
                            this,
                            // Include a request code to later monitor this update request.
                            MY_REQUEST_CODE);
                } catch (IntentSender.SendIntentException e) {
                    goHome();
                    e.printStackTrace();
                }
            } else {
                goHome();
            }
        });

        appUpdateManager.registerListener(listener);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MY_REQUEST_CODE) {
            if (resultCode != RESULT_OK) {
                Log.d("Update flow failed: ", "bb" + requestCode);
                // If the update is cancelled or fails,
                // you can request to start the update again.
            }
        }
    }

    InstallStateUpdatedListener listener = state -> {
        if (state.installStatus() == InstallStatus.DOWNLOADED) {
            // After the update is downloaded, show a notification
            // and request user confirmation to restart the app.
            popupSnackbarForCompleteUpdate();
        }
    };

    private void popupSnackbarForCompleteUpdate() {
        Snackbar snackbar =
                Snackbar.make(
                        findViewById(android.R.id.content),
                        "An update has just been downloaded.",
                        Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction("INSTALL", view -> appUpdateManager.completeUpdate());
        snackbar.setActionTextColor(
                getResources().getColor(android.R.color.white));
        snackbar.show();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (appUpdateManager != null) {
            appUpdateManager.unregisterListener(listener);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (appUpdateManager != null) {
            appUpdateManager
                    .getAppUpdateInfo()
                    .addOnSuccessListener(appUpdateInfo -> {
                        if (appUpdateInfo.installStatus() == InstallStatus.DOWNLOADED) {
                            popupSnackbarForCompleteUpdate();
                        }
                    });
        }

    }

    protected void goHome() {
        handler = new Handler();
        handler.postDelayed(runnable, TIMEOUT_MILLIS);
    }
}
